module.exports = {
  root: true,
  extends: ['custom', 'turbo'],
  settings: {
    next: {
      rootDir: ['apps/lab'],
    },
  },
};
