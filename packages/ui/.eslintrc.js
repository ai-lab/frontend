module.exports = {
  root: true,
  extends: ['custom'],
  ignorePatterns: ['!.storybook'],
  settings: {
    'import/resolver': {
      typescript: {},
    },
  },
};
