const path = require('path');

const tsTransformPaths = require('@zerollup/ts-transform-paths');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const { TsconfigPathsPlugin } = require('tsconfig-paths-webpack-plugin');

const urlLoader = {
  loader: 'url-loader',
  options: {
    limit: Infinity,
  },
};

const assetGenerator = {
  filename: path.join('./media', '[name][ext]'),
};

/** @type { import('webpack').Configuration } */
module.exports = {
  target: 'browserslist',
  entry: path.resolve(__dirname, 'src', 'index.ts'),
  output: {
    publicPath: './',
    path: path.resolve(__dirname, 'dist'),
    filename: 'index.js',
    globalObject: 'this',
    library: {
      name: '@ai-lab/ui',
      type: 'umd',
    },
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.jsx', '.js'],
    plugins: [new TsconfigPathsPlugin()],
    fallback: {
      path: require.resolve('path-browserify'),
    },
  },
  performance: {
    hints: false,
  },
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        minify: TerserPlugin.uglifyJsMinify,
      }),
      new CssMinimizerPlugin(),
    ],
    usedExports: true,
  },
  externals: [
    {
      react: {
        root: 'React',
        commonjs2: 'react',
        commonjs: 'react',
        amd: 'react',
      },
      'react-dom': {
        root: 'ReactDOM',
        commonjs2: 'react-dom',
        commonjs: 'react-dom',
        amd: 'react-dom',
      },
    },
  ],
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000,
    ignored: '**/node_modules',
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: {
          loader: 'ts-loader',
          options: {
            configFile: path.resolve(__dirname, 'tsconfig.lib.json'),
            getCustomTransformers: program => {
              const { before, afterDeclarations } = tsTransformPaths(program);
              return {
                before: [before],
                afterDeclarations: [afterDeclarations],
              };
            },
          },
        },
      },
      {
        test: /\.svg$/,
        oneOf: [
          {
            issuer: /\.(ts|js)x$/,
            use: {
              loader: '@svgr/webpack',
              options: {
                svgoConfig: {
                  plugins: [{ name: 'removeViewBox', active: false }],
                },
              },
            },
          },
          { type: 'asset/resource', generator: assetGenerator },
        ],
      },
      {
        test: /\.(ico|je?pg|a?png|gif|eot|otf|webp|ttf|woff2?|cur|ani|pdf)$/,
        oneOf: [
          { issuer: /\.(ts|js)x$/, use: urlLoader },
          { type: 'asset/resource', generator: assetGenerator },
        ],
      },
      {
        test: /\.(mp4|webm|wav|mp3|m4a|aac|oga)$/,
        oneOf: [
          { issuer: /\.(ts|js)x$/, use: urlLoader },
          { type: 'asset', generator: assetGenerator },
        ],
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          'style-loader',
          'css-loader',
          'postcss-loader',
          'resolve-url-loader',
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new CopyPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, 'src', 'assets'),
          to: path.resolve(__dirname, 'dist', 'assets'),
        },
      ],
    }),
  ],
};
