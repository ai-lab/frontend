import './locale-dropdown.scss';

import { Button, Dropdown } from 'antd';
import { DropdownProps } from 'antd/es/dropdown/dropdown';
import { MenuProps } from 'antd/es/menu/menu';

import CnFlag from 'assets/images/country-flags/cn.svg';
import RuFlag from 'assets/images/country-flags/ru.svg';
import UsFlag from 'assets/images/country-flags/us.svg';
import { Locale, useI18n } from 'stores/locale-store';
import { cn } from 'utils/class-name';
import { component } from 'utils/component';
import { Props, Icon } from 'utils/props-types';

export type LocaleDropdownProps = Props<{
  dropdown?: DropdownProps;
  onLocaleChange?: (locale: Locale) => void;
}>;

const localeLabels: { [locale in Locale]: string } = {
  [Locale.RU]: 'Русский',
  [Locale.EN]: 'English',
  [Locale.CN]: '中文版',
};

const localeFlags: { [locale in Locale]: Icon } = {
  [Locale.RU]: <RuFlag />,
  [Locale.EN]: <UsFlag />,
  [Locale.CN]: <CnFlag />,
};

const Flag = component<Props<{ locale: Locale }>>(
  ({ locale, className, ...restProps }) => {
    const { type: FlagIcon, props } = localeFlags[locale];
    return (
      <FlagIcon
        {...props}
        {...restProps}
        width={22}
        className={cn('al-locale-dropdown-icon', className, props.className)}
      />
    );
  },
);

export const LocaleDropdown = component<LocaleDropdownProps>(
  ({ dropdown, onLocaleChange, className, ...restProps }) => {
    const { i18n } = useI18n();

    const items: MenuProps['items'] = Object.values(Locale).map(
      (lc: Locale) => ({
        key: lc,
        label: localeLabels[lc],
        icon: <Flag locale={lc} className="al-locale-dropdown-icon--item" />,
        onClick: () => {
          i18n.changeLanguage(lc);
          onLocaleChange?.(lc);
        },
      }),
    );

    return (
      <Dropdown
        {...dropdown}
        menu={{ items, selectable: true, selectedKeys: [i18n.language] }}
        trigger={['click']}
        className={cn('al-locale-dropdown', className)}
        {...restProps}
      >
        <Button
          className="al-locale-dropdown__button"
          icon={<Flag locale={i18n.language as Locale} />}
        />
      </Dropdown>
    );
  },
);
