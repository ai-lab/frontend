import { Meta, Story } from '@storybook/react';
import { Typography } from 'antd';
import i18next from 'i18next';
import { useTranslation } from 'react-i18next';

import { LocaleDropdown, LocaleDropdownProps } from '.';

export default {
  title: 'Feedback/LocaleDropdown',
  component: LocaleDropdown,
} as Meta;

const Template: Story<LocaleDropdownProps> = args => (
  <LocaleDropdown {...args} />
);

export const Basic = Template.bind({});
Basic.args = {
  dropdown: {
    placement: 'bottom',
  },
};

export const Example = args => {
  const { t } = useTranslation('test');

  return (
    <div style={{ textAlign: 'center' }}>
      <Typography.Text style={{ display: 'block', marginBottom: '1rem' }}>
        {t('test')}
      </Typography.Text>
      <LocaleDropdown
        {...args}
        dropdown={{ placement: 'bottom' }}
        onLocaleChange={i18next.changeLanguage}
      />
    </div>
  );
};
