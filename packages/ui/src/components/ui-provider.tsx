import { FC, useEffect } from 'react';

import { ConfigProvider, theme as antdTheme } from 'antd';

import { antdLocales, useI18n } from 'stores/locale-store';
import { useTheme } from 'stores/theme-store';
import { WithChildren } from 'utils/props-types';

import 'antd/dist/reset.css';
import 'assets/scss/main.scss';

export type UiProviderProps = WithChildren;

export const UiProvider: FC<UiProviderProps> = ({ children }) => {
  const { i18n } = useI18n();
  const { isDark } = useTheme();

  useEffect(() => {
    if (isDark) document.body.classList.add('dark');
    else document.body.classList.remove('dark');
  }, [isDark]);

  return (
    <ConfigProvider
      theme={{
        algorithm: isDark
          ? antdTheme.darkAlgorithm
          : antdTheme.defaultAlgorithm,
      }}
      locale={antdLocales[i18n.language]}
    >
      {children}
    </ConfigProvider>
  );
};
