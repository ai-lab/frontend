import { FC, useEffect, useRef } from 'react';
import './page-layout.scss';

import { Footer, FooterProps } from 'components/content/footer';
import { cn } from 'utils/class-name';
import { Props } from 'utils/props-types';

import { Header, HeaderProps } from '../header';

export type PageLayoutProps = Props<
  {
    header: HeaderProps;
    footer?: FooterProps;
    contentClassName?: string;
  },
  true
>;

export const PageLayout: FC<PageLayoutProps> = ({
  header,
  footer,
  contentClassName,
  className,
  children,
}) => {
  const ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (!ref.current) return;
    ref.current.style.backgroundColor = 'var(--background-color-default)';
  }, []);

  return (
    <div ref={ref} className={cn('al-page-layout', className)}>
      <Header
        {...header}
        className={cn('al-page-layout__header', header.className)}
      />
      <main className={cn('al-page-layout__content', contentClassName)}>
        {children}
      </main>
      <Footer
        {...footer}
        className={cn('al-page-layout__footer', footer?.className)}
      />
    </div>
  );
};
