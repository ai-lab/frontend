import React from 'react';

import { Meta, Story } from '@storybook/react';
import i18next from 'i18next';
import { useTranslation } from 'react-i18next';

import { PageLayout, PageLayoutProps } from '.';

export default {
  title: 'Content/PageLayout',
  component: PageLayout,
} as Meta;

const Template: Story<PageLayoutProps> = args => {
  const { t } = useTranslation('header');

  return (
    <PageLayout
      {...args}
      header={{
        menu: {
          items: [
            {
              key: '0',
              label: t('imageRecognition'),
            },
          ],
          selectable: true,
          selectedKeys: ['0'],
        },
        localeDropdown: {
          onLocaleChange: i18next.changeLanguage,
        },
      }}
    />
  );
};

export const Basic = Template.bind({});
