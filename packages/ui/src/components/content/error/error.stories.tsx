import React from 'react';

import { Meta, Story } from '@storybook/react';

import { Error, ErrorProps } from '.';

export default {
  title: 'Content/Error',
  component: Error,
} as Meta;

const Template: Story<ErrorProps> = args => <Error {...args} />;

export const Basic404 = Template.bind({});
Basic404.args = {
  code: 404,
};

export const Basic500 = Template.bind({});
Basic500.args = {
  code: 500,
};

export const BasicOther = Template.bind({});
BasicOther.args = {
  code: 400,
};

export const BasicNoCode = Template.bind({});
