import './error.scss';

import { MouseEventHandler } from 'react';

import { Typography, Button } from 'antd';

import { useI18n } from 'stores/locale-store';
import { cn } from 'utils/class-name';
import { component } from 'utils/component';
import { Props } from 'utils/props-types';

const supportedErrorCodes = [404, 500];

export type ErrorProps = Props<{
  code?: number;
  onHomeClick?: MouseEventHandler<HTMLElement>;
}>;

export const Error = component<ErrorProps>(
  ({ code, onHomeClick, className, ...restProps }) => {
    const { t } = useI18n('error');

    const codeKey = String(
      code && supportedErrorCodes.includes(code) ? code : 'other',
    );

    return (
      <div className={cn('al-error', className)} {...restProps}>
        <Typography.Title className="al-error__code">
          {code ?? t('noCode')}
        </Typography.Title>
        <Typography.Text>{t(codeKey)}</Typography.Text>
        <Button
          className="al-error__button"
          type="primary"
          onClick={onHomeClick}
        >
          {t('home')}
        </Button>
      </div>
    );
  },
);
