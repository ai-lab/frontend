import React from 'react';
import './footer.scss';

import { Typography } from 'antd';

import { useI18n } from 'stores/locale-store';
import { useTheme } from 'stores/theme-store';
import { cn } from 'utils/class-name';
import { component } from 'utils/component';
import { Props } from 'utils/props-types';

export type FooterProps = Props;

export const Footer = component<FooterProps>(({ className, ...restProps }) => {
  const { isDark } = useTheme();
  const { t } = useI18n('footer');

  return (
    <footer
      className={cn('al-footer', { 'al-footer--dark': isDark }, className)}
      {...restProps}
    >
      <Typography.Text className="al-footer__coopyright">
        {t('copyright')}
      </Typography.Text>
    </footer>
  );
});
