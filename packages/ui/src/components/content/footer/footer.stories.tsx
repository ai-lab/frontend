import React from 'react';

import { Meta, Story } from '@storybook/react';

import { Footer, FooterProps } from '.';

export default {
  title: 'Content/Footer',
  component: Footer,
} as Meta;

const Template: Story<FooterProps> = args => <Footer {...args} />;

export const Basic = Template.bind({});
