import React from 'react';

import { Meta, Story } from '@storybook/react';
import i18next from 'i18next';
import { useTranslation } from 'react-i18next';

import { Header, HeaderProps } from '.';

export default {
  title: 'Content/Header',
  component: Header,
} as Meta;

const Template: Story<HeaderProps> = args => {
  const { t } = useTranslation('header');

  return (
    <Header
      {...args}
      menu={{
        items: [
          {
            key: '0',
            label: t('imageRecognition'),
          },
        ],
        selectable: true,
        selectedKeys: ['0'],
      }}
      localeDropdown={{
        onLocaleChange: i18next.changeLanguage,
      }}
    />
  );
};

export const Basic = Template.bind({});

export const LoggedIn = args => (
  <Basic loggedIn username="Ученик У." {...args} />
);
