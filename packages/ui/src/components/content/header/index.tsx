import React, { MouseEventHandler, ReactElement } from 'react';
import './header.scss';

import { Typography, Dropdown, Button } from 'antd';
import { MenuProps } from 'antd/es/menu/menu';
import { FiLogOut, FiMenu } from 'react-icons/fi';

import ItmoDark from 'assets/images/logos/itmo-dark.svg';
import ItmoLight from 'assets/images/logos/itmo-light.svg';
import MtsAiDark from 'assets/images/logos/mts-ai-dark.svg';
import MtsAiLight from 'assets/images/logos/mts-ai-light.svg';
import {
  ThemeSwitch,
  ThemeSwitchProps,
} from 'components/controls/theme-switch';
import {
  LocaleDropdown,
  LocaleDropdownProps,
} from 'components/feedback/locale-dropdown';
import { useI18n } from 'stores/locale-store';
import { useTheme } from 'stores/theme-store';
import { cn } from 'utils/class-name';
import { component } from 'utils/component';
import { Props } from 'utils/props-types';

export type HeaderProps = Props<{
  menu: MenuProps;
  titleLink?: ReactElement;
  loggedIn?: boolean;
  username?: string;
  onLogin?: MouseEventHandler<HTMLAnchorElement> &
    MouseEventHandler<HTMLButtonElement>;
  onLogout?: MouseEventHandler<SVGSVGElement>;
  localeDropdown?: LocaleDropdownProps;
  themeSwitch?: ThemeSwitchProps;
}>;

export const Header = component<HeaderProps>(
  ({
    menu,
    /* eslint-disable-next-line jsx-a11y/anchor-has-content,
       jsx-a11y/anchor-is-valid, jsx-a11y/control-has-associated-label */
    titleLink: TitleLink = <a href="#" />,
    loggedIn = false,
    username,
    onLogin,
    onLogout,
    localeDropdown,
    themeSwitch,
    className,
    ...restProps
  }) => {
    const { isDark } = useTheme();
    const { t } = useI18n('header');

    const selectedMenuItem =
      menu.selectedKeys
        ?.map(key => {
          const item: any = menu.items?.find(el => el?.key === key);
          return item?.label;
        })
        .join(', ') ?? null;

    return (
      <header className={cn('al-header', className)} {...restProps}>
        <div className="al-header__left">
          <a
            className="al-header__itmo"
            href="https://itmo.ru"
            target="_blank"
            rel="noreferrer"
          >
            {isDark ? <ItmoDark height={50} /> : <ItmoLight height={50} />}
          </a>
          <a
            className="al-header__mts-ai"
            href="https://mts.ai"
            target="_blank"
            rel="noreferrer"
          >
            {isDark ? <MtsAiDark height={30} /> : <MtsAiLight height={30} />}
          </a>
        </div>

        <div className="al-header__center">
          <TitleLink.type
            {...TitleLink.props}
            className={cn('al-header__link', TitleLink.props.className)}
          >
            <Typography.Title level={4} className="al-header__title">
              {t('title')}
            </Typography.Title>
          </TitleLink.type>
          <Dropdown
            className="al-header__menu"
            menu={menu}
            trigger={['click']}
            placement="bottom"
          >
            <Button
              className="al-header__menu-button"
              size="large"
              onClick={e => e.preventDefault()}
            >
              <Typography.Title className="al-header__menu-text" level={4}>
                {selectedMenuItem}
                <FiMenu />
              </Typography.Title>
            </Button>
          </Dropdown>
        </div>

        <div className="al-header__right">
          <div className="al-header__buttons">
            <LocaleDropdown
              dropdown={{ placement: 'bottom' }}
              {...localeDropdown}
            />
            <ThemeSwitch {...themeSwitch} />
          </div>

          <div className="al-header__profile">
            {loggedIn ? (
              <>
                <Typography.Title className="al-header__username" level={5}>
                  {username}
                </Typography.Title>
                <FiLogOut className="al-header__logout" onClick={onLogout} />
              </>
            ) : (
              <Button type="primary" onClick={onLogin}>
                {t('login')}
              </Button>
            )}
          </div>
        </div>
      </header>
    );
  },
);
