import './theme-switch.scss';

import { Button } from 'antd';
import { FiMoon, FiSun } from 'react-icons/fi';

import { Theme, useTheme } from 'stores/theme-store';
import { cn } from 'utils/class-name';
import { component } from 'utils/component';
import { Props } from 'utils/props-types';

export type ThemeSwitchProps = Props<{
  onThemeChange?: (theme: Theme) => void;
}>;

export const ThemeSwitch = component<ThemeSwitchProps>(
  ({ onThemeChange, className, ...restProps }) => {
    const { isDark, setTheme } = useTheme();

    return (
      <Button
        className={cn('al-theme-switch', className)}
        icon={isDark ? <FiMoon size={20} /> : <FiSun size={20} />}
        onClick={() => {
          const newTheme = isDark ? Theme.LIGHT : Theme.DARK;
          setTheme(newTheme);
          onThemeChange?.(newTheme);
        }}
        {...restProps}
      />
    );
  },
);
