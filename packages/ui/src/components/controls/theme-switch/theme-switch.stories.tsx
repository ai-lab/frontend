import { Meta, Story } from '@storybook/react';

import { useInitTheme } from 'stores/theme-store';

import { ThemeSwitch, ThemeSwitchProps } from '.';

export default {
  title: 'Controls/ThemeSwitch',
  component: ThemeSwitch,
} as Meta;

const Template: Story<ThemeSwitchProps> = args => {
  useInitTheme();
  return <ThemeSwitch {...args} />;
};

export const Basic = Template.bind({});
