import React, { useState } from 'react';

import { Meta, Story } from '@storybook/react';
import { UploadFile } from 'antd';

import { UploadGallery, UploadGalleryProps } from '.';

export default {
  title: 'Controls/UploadGallery',
  component: UploadGallery,
} as Meta;

const Template: Story<UploadGalleryProps> = args => {
  const [files, setFiles] = useState<UploadFile[]>([
    {
      uid: '-1',
      name: 'image.png',
      status: 'done',
      url: 'https://studiousguy.com/wp-content/uploads/2021/05/Biscuit.jpg',
    },
    {
      uid: '-xxx',
      percent: 50,
      name: 'image.png',
      status: 'uploading',
    },
  ]);

  return (
    <UploadGallery
      {...args}
      upload={{
        fileList: files,
        onChange: ({ fileList }) => setFiles(fileList),
      }}
    />
  );
};

export const Basic = Template.bind({});
