import './upload-gallery.scss';

import React from 'react';

import { PlusOutlined } from '@ant-design/icons';
import { Upload, UploadFile, UploadProps } from 'antd';
import { RcFile } from 'antd/es/upload';
import ImgCrop, { ImgCropProps } from 'antd-img-crop';

import { useI18n } from 'stores/locale-store';
import { cn } from 'utils/class-name';
import { component } from 'utils/component';
import { Props } from 'utils/props-types';

export type UploadGalleryProps = Props<{
  crop?: ImgCropProps;
  upload?: UploadProps;
}>;

export const UploadGallery = component<UploadGalleryProps>(
  ({ crop, upload, className, ...restProps }) => {
    const { t } = useI18n('upload-gallery');

    const onPreview = async (file: UploadFile) => {
      let src = (file.preview ?? file.url) as string;

      if (src && !src.startsWith('data:image')) {
        window.open(src);
        return;
      }

      if (!src) {
        src = await new Promise(resolve => {
          const reader = new FileReader();
          reader.readAsDataURL(file.originFileObj as RcFile);
          reader.onload = () => resolve(reader.result as string);
        });
      }

      const image = new Image();
      image.src = src;
      const imgWindow = window.open(file.url);
      imgWindow?.document.write(image.outerHTML);
    };

    return (
      <div
        className={cn(
          'al-upload-gallery',
          { 'al-upload-gallery--not-empty': upload?.fileList?.length !== 0 },
          className,
        )}
        {...restProps}
      >
        <ImgCrop
          modalTitle={t('editModalTitle') ?? ''}
          modalClassName="al-crop-modal"
          rotationSlider
          {...crop}
        >
          <Upload listType="picture-card" onPreview={onPreview} {...upload}>
            <PlusOutlined className="al-upload-gallery__plus" />
          </Upload>
        </ImgCrop>
      </div>
    );
  },
);
