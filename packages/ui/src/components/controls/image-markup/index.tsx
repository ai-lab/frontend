import './image-markup.scss';

import { ReactElement, useEffect, useRef } from 'react';

import { cn } from 'utils/class-name';
import { component } from 'utils/component';
import { addMoveListener, Coordinates } from 'utils/move-listener';
import { Props } from 'utils/props-types';
import { getStyleValue } from 'utils/style-number';

export type Markup = {
  from: Coordinates;
  to: Coordinates;
};

export type ImageMarkupProps = Props<{
  image: ReactElement;
  markup?: Markup;
  onMarkup?: (markup: Markup) => void;
  enabled?: boolean;
}>;

export const ImageMarkup = component<ImageMarkupProps>(
  ({
    image: Image,
    markup,
    onMarkup,
    enabled = true,
    className,
    ...restProps
  }) => {
    const ref = useRef<HTMLDivElement>(null);
    const markupRef = useRef<HTMLDivElement>(null);

    const getMarkup = (): Markup | undefined => {
      const left = getStyleValue(markupRef, 'left');
      const top = getStyleValue(markupRef, 'top');
      if (top === undefined || left === undefined) return undefined;

      const from: Coordinates = { x: left, y: top };

      const width = getStyleValue(markupRef, 'width');
      const height = getStyleValue(markupRef, 'height');
      if (width === undefined || height === undefined) return undefined;

      return {
        from,
        to: {
          x: from.x + width,
          y: from.y + height,
        },
      };
    };

    const setMarkup = (set: Markup | ((prev: Markup) => Markup)) => {
      if (!ref.current) return;

      const getNewMarkup = () => {
        if (typeof set === 'function') {
          const prev = getMarkup();
          return prev && set(prev);
        }
        return set;
      };

      const newMarkup = getNewMarkup();
      if (!newMarkup) return;
      const { from, to } = newMarkup;

      if (from.x < 0) from.x = 0;
      if (from.y < 0) from.y = 0;

      if (to.x > ref.current.clientWidth) {
        to.x = ref.current.clientWidth;
      }
      if (to.y > ref.current.clientHeight) {
        to.y = ref.current.clientHeight;
      }

      requestAnimationFrame(() => {
        if (!markupRef.current) return;
        markupRef.current.style.setProperty('left', `${from.x}px`);
        markupRef.current.style.setProperty('top', `${from.y}px`);
        markupRef.current.style.setProperty('width', `${to.x - from.x}px`);
        markupRef.current.style.setProperty('height', `${to.y - from.y}px`);
      });
    };

    useEffect(() => {
      if (markup) {
        setMarkup(markup);
      }
    }, [markup]);

    const image = useRef<HTMLDivElement>(null);

    useEffect(() => {
      const setupMarkup = () => {
        if (!ref.current || markup) return;
        const { width, height } = ref.current.getBoundingClientRect();
        setMarkup({
          from: { x: width / 4, y: height / 4 },
          to: { x: (width / 4) * 3, y: (height / 4) * 3 },
        });
      };

      setupMarkup();
      image.current?.addEventListener('load', setupMarkup);
      return () => image.current?.removeEventListener('load', setupMarkup);
    }, []);

    const topLeft = useRef<HTMLDivElement>(null);
    const topRight = useRef<HTMLDivElement>(null);
    const bottomLeft = useRef<HTMLDivElement>(null);
    const bottomRight = useRef<HTMLDivElement>(null);

    const onRelease = () => {
      const currentMarkup = getMarkup();

      if (currentMarkup) {
        onMarkup?.(currentMarkup);
      }
    };

    useEffect(() => {
      if (!enabled) return;

      const removeListeners = [
        addMoveListener({
          container: ref,
          target: topLeft,
          onMove: from => setMarkup(prev => ({ ...prev, from })),
          onRelease,
        }),
        addMoveListener({
          container: ref,
          target: topRight,
          onMove: ({ x, y }) =>
            setMarkup(prev => ({
              from: { x: prev.from.x, y },
              to: { x, y: prev.to.y },
            })),
          onRelease,
        }),
        addMoveListener({
          container: ref,
          target: bottomLeft,
          onMove: ({ x, y }) =>
            setMarkup(prev => ({
              from: { x, y: prev.from.y },
              to: { x: prev.to.x, y },
            })),
          onRelease,
        }),
        addMoveListener({
          container: ref,
          target: bottomRight,
          onMove: to => setMarkup(prev => ({ ...prev, to })),
          onRelease,
        }),
      ];

      // eslint-disable-next-line consistent-return
      return () => removeListeners.forEach(remove => remove());
    }, []);

    useEffect(() => {
      if (!enabled) return;

      let clickMouse: Coordinates | undefined;
      let clickMarkup: Markup | undefined;

      // eslint-disable-next-line consistent-return
      return addMoveListener({
        container: ref,
        target: markupRef,
        onGrab: e => {
          if (!ref.current) return;
          clickMarkup = getMarkup();
          clickMouse = {
            x: e.clientX - ref.current.offsetLeft,
            y: e.clientY - ref.current.offsetTop,
          };
        },
        onMove: ({ x, y }) => {
          if (!clickMouse || !clickMarkup) return;
          setMarkup({
            from: {
              x: clickMarkup.from.x - (clickMouse.x - x) + 1,
              y: clickMarkup.from.y - (clickMouse.y - y) + 1,
            },
            to: {
              x: clickMarkup.to.x - (clickMouse.x - x) + 1,
              y: clickMarkup.to.y - (clickMouse.y - y) + 1,
            },
          });
        },
        onRelease,
      });
    }, []);

    return (
      <div
        ref={ref}
        className={cn('al-image-markup', className)}
        {...restProps}
      >
        <Image.type
          {...Image.props}
          ref={image}
          className={cn('al-image-markup__image', Image.props.className)}
        />

        <div ref={markupRef} className="al-image-markup__markup">
          <div ref={topLeft} className="al-image-markup__top-left" />
          <div ref={topRight} className="al-image-markup__top-right" />
          <div ref={bottomLeft} className="al-image-markup__bottom-left" />
          <div ref={bottomRight} className="al-image-markup__bottom-right" />
        </div>
      </div>
    );
  },
);
