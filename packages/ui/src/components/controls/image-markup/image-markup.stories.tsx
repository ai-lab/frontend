import { Meta, Story } from '@storybook/react';

import { ImageMarkup, ImageMarkupProps } from '.';

export default {
  title: 'Controls/ImageMarkup',
  component: ImageMarkup,
} as Meta;

const Template: Story<ImageMarkupProps> = args => {
  const image = (
    <img
      src="https://studiousguy.com/wp-content/uploads/2021/05/Biscuit.jpg"
      alt="test"
    />
  );

  return <ImageMarkup {...args} image={image} />;
};

export const Basic = Template.bind({});
