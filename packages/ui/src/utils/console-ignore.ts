export const consoleIgnore = (typesAndMessages: {
  [messageType in keyof Console]?: string[];
}) => {
  Object.keys(typesAndMessages).forEach(type => {
    // eslint-disable-next-line no-console
    const prevFunction = console[type];
    // eslint-disable-next-line no-console
    console[type] = function filterWarnings(msg, ...args) {
      if (!typesAndMessages[type].some(entry => msg.includes(entry))) {
        prevFunction(msg, ...args);
      }
    };
  });
};
