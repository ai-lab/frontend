import { RefObject } from 'react';

export const getStyleValue = <T extends HTMLElement>(
  ref: RefObject<T>,
  name: string,
): number | undefined => {
  if (!ref.current) return undefined;
  const value = Number(
    ref.current.style.getPropertyValue(name).replace('px', ''),
  );
  if (Number.isNaN(value)) return undefined;
  return value;
};
