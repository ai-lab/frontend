import { RefObject } from 'react';

export type Coordinates = {
  x: number;
  y: number;
};

export type MoveListenerOptions = {
  container: RefObject<HTMLDivElement>;
  target: RefObject<HTMLDivElement>;
  onGrab?: (e: MouseEvent) => void;
  onMove?: (coordinates: Coordinates) => void;
  onRelease?: (e: MouseEvent) => void;
};

export const addMoveListener = ({
  container,
  target,
  onGrab,
  onMove,
  onRelease,
}: MoveListenerOptions) => {
  const onMouseMove = (e: MouseEvent) => {
    if (!container.current) return;
    const x = e.clientX - container.current.offsetLeft;
    const y = e.clientY - container.current.offsetTop;
    onMove?.({ x, y });
  };

  const onMouseUp = (e: MouseEvent) => {
    onRelease?.(e);
    if (e.defaultPrevented) return;
    target.current?.style.removeProperty('cursor');
    window.removeEventListener('mousemove', onMouseMove);
  };

  const onMouseDown = (e: MouseEvent) => {
    onGrab?.(e);
    if (e.defaultPrevented) return;
    e.stopImmediatePropagation();
    target.current?.style.setProperty('cursor', 'grabbing');
    if (!container.current || !target.current) return;
    window.addEventListener('mousemove', onMouseMove);
    window.addEventListener('mouseup', onMouseUp, { once: true });
  };

  target.current?.addEventListener('mousedown', onMouseDown);

  return () => {
    target.current?.removeEventListener('mousedown', onMouseDown);
    window.removeEventListener('mousemove', onMouseMove);
    window.removeEventListener('mouseup', onMouseUp);
  };
};
