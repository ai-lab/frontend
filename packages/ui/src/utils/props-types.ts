import {
  CSSProperties,
  PropsWithChildren,
  ReactElement,
  SVGProps,
} from 'react';

export type IdProps = {
  id?: string;
};

export type ClassNameProps = {
  className?: string;
};

export type StyleProps = {
  style?: CSSProperties;
};

export type DefaultProps = ClassNameProps & StyleProps & IdProps;

export type WithChildren<Props = {}> = PropsWithChildren<Props>;

export type Icon = ReactElement<SVGProps<SVGSVGElement>>;

export type Props<
  ComponentProps = {},
  Children = false,
> = Children extends false
  ? Omit<DefaultProps & ComponentProps, 'children'>
  : Children extends true
  ? WithChildren<DefaultProps & ComponentProps>
  : { children: Children } & DefaultProps & ComponentProps;
