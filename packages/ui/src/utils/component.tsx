import {
  FC,
  forwardRef,
  ForwardRefRenderFunction,
  memo,
  PropsWithRef,
} from 'react';

export const component = <Props = {},>(render: FC<Props>) =>
  memo(render) as unknown as FC<Props>;

export const componentWithRef = <Props = {}, Ref = unknown>(
  render: ForwardRefRenderFunction<Ref, Props>,
) => component(forwardRef(render)) as FC<PropsWithRef<Props>>;
