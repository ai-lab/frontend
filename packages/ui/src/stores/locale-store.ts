import { Locale as AntdLocale } from 'antd/es/locale';
import enUS from 'antd/locale/en_US';
import ruRU from 'antd/locale/ru_RU';
import znCN from 'antd/locale/zh_CN';
import { Namespace, KeyPrefix } from 'i18next';
import {
  UseTranslationResponse,
  UseTranslationOptions,
  useTranslation as useReactTranslation,
} from 'react-i18next';
import { DefaultNamespace } from 'react-i18next/TransWithoutContext';
import { create } from 'zustand';

export enum Locale {
  RU = 'ru',
  EN = 'en',
  CN = 'cn',
}

export const antdLocales: { [locale in Locale]: AntdLocale } = {
  [Locale.RU]: ruRU,
  [Locale.EN]: enUS,
  [Locale.CN]: znCN,
};

type LocaleState = {
  useTranslation: typeof useReactTranslation;
};

type LocaleActions = {
  setUseTranslation: (locale: typeof useReactTranslation) => void;
};

export const useLocale = create<LocaleState & LocaleActions>(set => ({
  useTranslation: useReactTranslation,
  setUseTranslation: useTranslation => set({ useTranslation }),
}));

export const useI18n = <
  N extends Namespace = DefaultNamespace,
  TKPrefix extends KeyPrefix<N> = undefined,
>(
  ns?: N | Readonly<N>,
  options?: UseTranslationOptions<TKPrefix>,
): UseTranslationResponse<N, TKPrefix> =>
  useLocale(state => state.useTranslation)(ns, options);

export const availableNamespaces = [
  'common',
  'error',
  'footer',
  'header',
  'test',
  'upload-gallery',
];
