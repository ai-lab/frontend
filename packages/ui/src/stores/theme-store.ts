import { useEffect } from 'react';

import { create } from 'zustand';

export enum Theme {
  LIGHT = 'light',
  DARK = 'dark',
}

type ThemeState = {
  theme: Theme;
  isDark: boolean;
};

type ThemeActions = {
  setTheme: (theme: Theme) => void;
};

const defaultTheme: Theme = Theme.LIGHT;

export const useTheme = create<ThemeState & ThemeActions>(set => ({
  theme: defaultTheme,
  // @ts-ignore
  isDark: defaultTheme === Theme.DARK,
  setTheme: theme => set({ theme, isDark: theme === Theme.DARK }),
}));

export const detectTheme = () =>
  window.matchMedia('(prefers-color-scheme:light)').matches
    ? Theme.LIGHT
    : Theme.DARK;

export const useInitTheme = () => {
  const { setTheme } = useTheme();

  useEffect(() => {
    const match = window.matchMedia('(prefers-color-scheme:light)');

    const onColorScheme = e => {
      setTheme(e.matches ? Theme.LIGHT : Theme.DARK);
    };

    onColorScheme(match);
    match.addEventListener('change', onColorScheme);
    return () => match.removeEventListener('change', onColorScheme);
  }, [setTheme]);
};
