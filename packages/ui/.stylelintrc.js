module.exports = {
  extends: ['stylelint-config-custom'],
  ignoreFiles: [
    "node_modules/**/*",
    "dist/**/*"
  ]
};
