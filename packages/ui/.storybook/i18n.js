import i18n from 'i18next';
import HttpApi from 'i18next-http-backend';

import { availableNamespaces } from '../src/stores/locale-store';

i18n.use(HttpApi).init({
  lng: 'ru',
  supportedLngs: ['ru', 'en', 'cn'],
  fallbackLng: 'en',
  ns: availableNamespaces,
});

export default i18n;
