const { TsconfigPathsPlugin } = require('tsconfig-paths-webpack-plugin');

module.exports = {
  stories: ['../src/**/*.stories.@(ts|tsx|js|jsx|mdx)'],
  addons: [
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    '@storybook/addon-interactions',
    '@storybook/preset-scss',
    '@storybook/addon-postcss',
  ],
  framework: '@storybook/react',
  core: {
    builder: '@storybook/builder-webpack5',
    disableTelemetry: true,
  },
  features: {
    storyStoreV7: true,
  },
  staticDirs: ['../public'],
  webpackFinal: async config => {
    // eslint-disable-next-line no-param-reassign
    config.resolve.plugins = [
      ...(config.resolve.plugins || []),
      new TsconfigPathsPlugin(),
    ];
    const imageRule = config.module.rules.find(rule => rule.test?.test('.svg'));
    imageRule.exclude = /\.svg$/;
    config.module.rules.push({
      test: /\.svg$/,
      oneOf: [
        {
          issuer: /\.(ts|js)x$/,
          exclude: /fonts/,
          use: {
            loader: '@svgr/webpack',
            options: {
              svgoConfig: {
                plugins: [{ name: 'removeViewBox', active: false }],
              },
            },
          },
        },
        imageRule,
      ],
    });
    return config;
  },
};
