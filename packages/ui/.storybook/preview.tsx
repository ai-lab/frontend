import React, { useEffect } from 'react';

import { I18nextProvider } from 'react-i18next';

import i18nConfig from './i18n';
import { UiProvider } from '../src/components/ui-provider';
import { useI18n } from '../src/stores/locale-store';
import { useTheme } from '../src/stores/theme-store';
import { component } from '../src/utils/component';
import { consoleIgnore } from '../src/utils/console-ignore';
import { WithChildren } from '../src/utils/props-types';

export const parameters = {
  layout: 'centered',
  actions: { argTypesRegex: '^on.+' },
};

export const globalTypes = {
  theme: {
    description: 'Theme for components',
    defaultValue: 'light',
    toolbar: {
      dynamicTitle: true,
      items: [
        { value: 'light', title: 'Light', icon: 'circlehollow' },
        { value: 'dark', title: 'Dark', icon: 'circle' },
      ],
    },
  },
  locale: {
    description: 'Interface locale',
    defaultValue: 'ru',
    toolbar: {
      icon: 'globe',
      dynamicTitle: true,
      items: [
        { value: 'ru', title: 'Русский', left: '🇺🇸' },
        { value: 'en', title: 'English', left: '🇷🇺' },
        { value: 'cn', title: '中文版', left: '🇨🇳' },
      ],
    },
  },
};

export const decorators = [
  (Story, context) => {
    const params = { ...context.parameters, ...context.globals };

    const ConfigProvider = component<WithChildren>(({ children }) => {
      const { i18n } = useI18n();
      useEffect(() => {
        i18n.changeLanguage(params.locale);
      }, [params.locale]);

      const { setTheme } = useTheme();
      useEffect(() => {
        setTheme(params.theme);
      }, [params.theme]);

      return (
        <div
          style={{
            backgroundColor: 'var(--background-color-default)',
            transition: 'var(--transition-default)',
            minWidth: 'calc(100vw - 2rem)',
            height: 'calc(100vh - 2rem)',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          {children}
        </div>
      );
    });

    return (
      <I18nextProvider i18n={i18nConfig}>
        <UiProvider>
          <ConfigProvider>
            <Story />
          </ConfigProvider>
        </UiProvider>
      </I18nextProvider>
    );
  },
];

consoleIgnore({
  error: ['ReactDOM'],
});
