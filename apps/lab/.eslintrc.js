module.exports = {
  root: true,
  extends: ['custom', 'plugin:@next/next/recommended'],
  settings: {
    'import/resolver': {
      typescript: {},
    },
  },
};
