import { FormData } from 'next/dist/compiled/@edge-runtime/primitives/fetch';

import { ExternalId, Pageable } from 'model/common';
import { FirmwareDto } from 'model/firmware';
import { ObjectImageDto } from 'model/object-image';
import { backendClient } from 'utils/api-clients';

export const firmwaresApi = {
  getFirmwareIds: (pageable?: Pageable) =>
    backendClient.get<ExternalId[]>('/api/v1/firmwares', {
      params: { ...pageable },
    }),

  getFirmware: (id: ExternalId) =>
    backendClient.get<FirmwareDto>(`/api/v1/firmwares/${id}`),

  addFirmware: (firmware: File, name: string) => {
    const formData = new FormData();
    formData.set('firmware', firmware);
    return backendClient.post<ObjectImageDto>(
      '/api/v1/object-images',
      formData,
      { params: { name } },
    );
  },

  deleteFirmware: (id: ExternalId) =>
    backendClient.delete(`/api/v1/firmwares/${id}`),
};
