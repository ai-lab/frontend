import { AxiosRequestConfig } from 'axios';

import { ExternalId, Pageable, PageDto } from 'model/common';
import { ObjectFrameDto, ObjectImageDto } from 'model/object-image';
import { backendClient } from 'utils/api-clients';

export const objectImagesApi = {
  getObjectImageIds: (pageable?: Pageable) =>
    backendClient.get<PageDto<ExternalId>>('/api/v1/object-images', {
      params: { ...pageable },
    }),

  getObjectImage: (id: ExternalId) =>
    backendClient.get<ObjectImageDto>(`/api/v1/object-images/${id}`),

  downloadObjectImage: (id: ExternalId, config?: AxiosRequestConfig) =>
    backendClient.get<Blob>(`/api/v1/object-images/${id}/download`, {
      responseType: 'blob',
      ...config,
    }),

  addObjectImage: (image: File, config?: AxiosRequestConfig) => {
    const formData = new FormData();
    formData.set('image', image);
    return backendClient.post<ObjectImageDto>(
      '/api/v1/object-images',
      formData,
      config,
    );
  },

  updateObjectFrame: (id: ExternalId, objectFrame: ObjectFrameDto) =>
    backendClient.patch<ObjectImageDto>(
      `/api/v1/object-images/${id}`,
      objectFrame,
    ),

  deleteObjectImage: (id: ExternalId) =>
    backendClient.delete(`/api/v1/object-images/${id}`),
};
