import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

import { ErrorLayout } from 'components/layout/error-layout';
import { nextPage } from 'utils/next-page';

export const getStaticProps = async ({ locale = 'ru' }) => ({
  props: await serverSideTranslations(locale, ['header', 'footer', 'error']),
});

const Custom404 = nextPage(() => <ErrorLayout code={404} />);

export default Custom404;
