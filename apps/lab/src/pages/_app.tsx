import { UiProvider, useLocale } from '@ai-lab/ui';
import { QueryClientProvider } from '@tanstack/react-query';
import App, { AppContext, AppProps } from 'next/app';
import { Session } from 'next-auth';
import { getSession, SessionProvider } from 'next-auth/react';
import { appWithTranslation, useTranslation } from 'next-i18next';
import { ThemeProvider } from 'next-themes';

import { SessionErrorHandler } from 'components/utils/session-error-handler';
import { ThemeDetector } from 'components/utils/theme-detector';
import queryClient from 'config/query-client';

import i18nextConfig from '../../next-i18next.config';

useLocale.setState(state => ({ ...state, useTranslation }));

type AiLabAppProps = AppProps & {
  session: Session;
};

const AiLabApp = ({
  Component,
  pageProps,
  session,
}: AiLabAppProps): JSX.Element => {
  useLocale.setState(state => ({ ...state, useTranslation }));

  return (
    <QueryClientProvider client={queryClient}>
      <SessionProvider
        session={session}
        refetchInterval={process.env.NEXT_PUBLIC_SESSION_REFETCH_INTERVAL}
      >
        <SessionErrorHandler>
          <ThemeProvider>
            <ThemeDetector>
              <UiProvider>
                <Component {...pageProps} />
              </UiProvider>
            </ThemeDetector>
          </ThemeProvider>
        </SessionErrorHandler>
      </SessionProvider>
    </QueryClientProvider>
  );
};

// Initializes session on each page
AiLabApp.getInitialProps = async (context: AppContext) => ({
  ...(await App.getInitialProps(context)),
  session: await getSession(context.ctx),
});

export default appWithTranslation(AiLabApp, i18nextConfig);
