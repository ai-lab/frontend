import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

import { ErrorLayout } from 'components/layout/error-layout';
import { nextPage } from 'utils/next-page';

export const getStaticProps = async ({ locale = 'ru' }) => ({
  props: await serverSideTranslations(locale, ['header', 'footer', 'error']),
});

const Custom500 = nextPage(() => <ErrorLayout code={500} />);

export default Custom500;
