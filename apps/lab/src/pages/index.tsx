import React from 'react';

import './index.scss';

import { Typography, useI18n } from '@ai-lab/ui';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

import { MainLayout } from 'components/layout/main-layout';
import { nextPage } from 'utils/next-page';

export const getStaticProps = async ({ locale = 'ru' }) => ({
  props: await serverSideTranslations(locale, ['header', 'footer', 'home']),
});

const Home = nextPage(() => {
  const { t } = useI18n('home');

  return (
    <MainLayout contentClassName="al-home-page">
      <div className="al-home-page__centered">
        <Typography.Title>{t('welcome')}</Typography.Title>
        <Typography.Text>{t('continue')}</Typography.Text>
      </div>
    </MainLayout>
  );
});

export default Home;
