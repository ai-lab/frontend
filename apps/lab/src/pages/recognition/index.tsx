import React from 'react';
import './recognition.scss';

import { Typography, useI18n, Button } from '@ai-lab/ui';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

import { MainLayout } from 'components/layout/main-layout';
import { useUser } from 'hooks/use-user';
import { nextPage } from 'utils/next-page';

export const getStaticProps = async ({ locale = 'ru' }) => ({
  props: await serverSideTranslations(locale, [
    'header',
    'footer',
    'recognition',
    'utils',
  ]),
});

const Recognition = nextPage(() => {
  const { t } = useI18n('recognition');
  const { t: tu } = useI18n('utils');
  const router = useRouter();
  const { authenticated } = useUser();

  if (authenticated) {
    if (typeof window !== 'undefined') {
      router.replace('/recognition/upload');
    }

    return null;
  }

  return (
    <MainLayout contentClassName="al-recognition-page">
      <div className="al-recognition-page__centered">
        <div className="al-recognition-page__left">
          <Typography.Title>{t('title')}</Typography.Title>
          <Typography.Text>{t('description')}</Typography.Text>
          <Button
            className="al-recognition-page__button"
            type="primary"
            onClick={() => router.push('/recognition/upload')}
          >
            {tu('start')}
          </Button>
        </div>
        <Image
          className="al-recognition-page__right"
          src="/images/recognition.png"
          width="452"
          height="277"
          alt={t('title')}
        />
      </div>
    </MainLayout>
  );
});

export default Recognition;
