import React, { useState, useRef, useEffect } from 'react';
import './testing.scss';

import {
  Alert,
  InboxOutlined,
  Upload,
  useI18n,
  ImageMarkup,
  Button,
  Spin,
} from '@ai-lab/ui';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { UploadRequestOption, RcFile } from 'rc-upload/lib/interface';

import { MainLayout } from 'components/layout/main-layout';
import { RecognitionLayout } from 'components/layout/recognition-layout';
import { nextPage } from 'utils/next-page';

export const getStaticProps = async ({ locale = 'ru' }) => ({
  props: await serverSideTranslations(locale, [
    'header',
    'footer',
    'recognition',
    'utils',
    'testing',
  ]),
});

const objectClassClassName = 'al-testing-page__object-class';

const Testing = nextPage(() => {
  const { t } = useI18n('testing');
  const [isLoading, setIsLoading] = useState(false);
  const [image, setImage] = useState<File>();

  const timeouts = useRef<NodeJS.Timeout[]>([]);
  useEffect(() => () => timeouts.current.forEach(clearTimeout), []);

  const customRequest = ({ file, onSuccess }: UploadRequestOption) => {
    setIsLoading(true);
    timeouts.current.push(
      setTimeout(() => {
        onSuccess?.(file);
        setImage(file as RcFile);
        setIsLoading(false);
      }, 1000),
    );
  };

  useEffect(() => {
    if (image) {
      const objectClassElement = document.createElement('div');
      objectClassElement.innerHTML = 'Дерево';
      objectClassElement.setAttribute('class', objectClassClassName);
      document
        .querySelector('.al-image-markup__markup')
        ?.append(objectClassElement);
    }

    return () => document.querySelector(`.${objectClassClassName}`)?.remove();
  }, [image]);

  return (
    <MainLayout isPrivate contentClassName="al-testing-page">
      <RecognitionLayout currentStep={3}>
        <div className="al-testing-page__content">
          <Alert
            className="al-testing-page__instruction"
            description={t('testingInstruction')}
          />
          {isLoading && (
            <div className="al-testing-page__loader">
              <Spin />
            </div>
          )}

          {!isLoading && image && (
            <div className="al-testing-page__result">
              <ImageMarkup
                className="al-testing-page__markup"
                enabled={false}
                markup={{
                  from: {
                    x: 247,
                    y: 274,
                  },
                  to: {
                    x: 366,
                    y: 355,
                  },
                }}
                image={
                  <img
                    className="al-testing-page__image"
                    src={URL.createObjectURL(image)}
                    alt={image.name}
                  />
                }
              />
              <Button onClick={() => setImage(undefined)}>
                {t('selectOtherImage')}
              </Button>
            </div>
          )}

          {!isLoading && !image && (
            <Upload.Dragger
              className="al-testing-page__upload"
              accept="image/*"
              showUploadList={false}
              customRequest={customRequest}
            >
              <p className="ant-upload-drag-icon">
                <InboxOutlined />
              </p>
              <p className="ant-upload-text">{t('uploadAreaTitle')}</p>
              <p className="ant-upload-hint">{t('uploadAreaDescription')}</p>
            </Upload.Dragger>
          )}
        </div>
      </RecognitionLayout>
    </MainLayout>
  );
});

export default Testing;
