import React, { useState, useEffect, useRef } from 'react';
import './training.scss';

import {
  Alert,
  useI18n,
  Button,
  Result,
  Progress,
  Form,
  Tooltip,
  InputNumber,
  Typography,
} from '@ai-lab/ui';
import { useRouter } from 'next/router';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

import { MainLayout } from 'components/layout/main-layout';
import { RecognitionLayout } from 'components/layout/recognition-layout';
import { nextPage } from 'utils/next-page';

export const getStaticProps = async ({ locale = 'ru' }) => ({
  props: await serverSideTranslations(locale, [
    'header',
    'footer',
    'recognition',
    'utils',
    'training',
  ]),
});

type TrainingParameters = {
  batch: number;
  epoch: number;
  workers: number;
};

const percents = [
  {
    percent: 0,
    duration: 500,
  },
  {
    percent: 30,
    duration: 800,
  },
  {
    percent: 53,
    duration: 1000,
  },
  {
    percent: 75,
    duration: 800,
  },
  {
    percent: 90,
    duration: 500,
  },
  {
    percent: 100,
    duration: 800,
  },
];

const Training = nextPage(() => {
  const { t } = useI18n('training');
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(false);
  const [trainingPercent, setTrainingPercent] = useState<number>();

  const timeouts = useRef<NodeJS.Timeout[]>([]);
  useEffect(() => () => timeouts.current.forEach(clearTimeout), []);

  const setPercent = (i: number = 0) => {
    if (i > percents.length - 1) return;
    timeouts.current.push(
      setTimeout(() => {
        setTrainingPercent(percents[i].percent);
        setPercent(i + 1);
      }, percents[i].duration),
    );
  };

  const onSubmit = (parameters: TrainingParameters) => {
    // eslint-disable-next-line no-console
    console.log({ parameters });
    setIsLoading(true);
    timeouts.current.push(
      setTimeout(() => {
        setIsLoading(false);
        setPercent();
      }, 500),
    );
  };

  return (
    <MainLayout isPrivate contentClassName="al-training-page">
      <RecognitionLayout currentStep={2}>
        <div className="al-training-page__content">
          <Alert
            className="al-training-page__instruction"
            description={t('trainingInstruction')}
          />

          {trainingPercent === undefined && (
            <Form
              className="al-training-page__form"
              layout="vertical"
              autoComplete="off"
              onFinish={onSubmit}
            >
              <Tooltip title={t('batchDescription')}>
                <Form.Item
                  label={t('batchLabel')}
                  name="batch"
                  rules={[{ required: true, type: 'number', min: 1, max: 5 }]}
                >
                  <InputNumber placeholder={t('batchRecommended') ?? ''} />
                </Form.Item>
              </Tooltip>

              <Tooltip title={t('epochDescription')}>
                <Form.Item
                  label={t('epochLabel')}
                  name="epoch"
                  rules={[
                    { required: true, type: 'number', min: 1, max: 2000 },
                  ]}
                >
                  <InputNumber placeholder={t('epochRecommended') ?? ''} />
                </Form.Item>
              </Tooltip>

              <Tooltip title={t('workersDescription')}>
                <Form.Item
                  label={t('workersLabel')}
                  name="workers"
                  rules={[{ required: true, type: 'number', min: 1, max: 5 }]}
                >
                  <InputNumber placeholder={t('workersRecommended') ?? ''} />
                </Form.Item>
              </Tooltip>

              <div className="al-training-page__start-button-wrapper">
                <Button
                  className="al-training-page__start-button"
                  size="large"
                  type="primary"
                  htmlType="submit"
                  loading={isLoading}
                >
                  {t('startTraining')}
                </Button>
              </div>
            </Form>
          )}

          {trainingPercent !== undefined && trainingPercent !== 100 && (
            <div className="al-training-page__progress">
              <Progress
                type="circle"
                percent={trainingPercent}
                strokeColor={{ '0%': '#108ee9', '100%': '#87d068' }}
              />
              <Typography.Text>{t('trainingInProgress')}</Typography.Text>
            </div>
          )}

          {trainingPercent === 100 && (
            <Result
              status="success"
              title={t('trainingSuccessful')}
              subTitle={t('canContinue')}
              extra={[
                <Button
                  key="startAgain"
                  onClick={() => setTrainingPercent(undefined)}
                >
                  {t('startAgain')}
                </Button>,
                <Button
                  key="testing"
                  type="primary"
                  onClick={() => router.push('/recognition/testing')}
                >
                  {t('testing')}
                </Button>,
              ]}
            />
          )}
        </div>
      </RecognitionLayout>
    </MainLayout>
  );
});

export default Training;
