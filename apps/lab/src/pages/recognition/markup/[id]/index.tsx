import React, { useEffect, useState } from 'react';
import './markup.scss';

import {
  ImageMarkup,
  Spin,
  Button,
  useI18n,
  Markup,
  Input,
  Space,
  Alert,
  message,
} from '@ai-lab/ui';
import { useQuery, useMutation } from '@tanstack/react-query';
import { useRouter } from 'next/router';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { FaChevronLeft, FaChevronRight } from 'react-icons/fa';

import { MainLayout } from 'components/layout/main-layout';
import { RecognitionLayout } from 'components/layout/recognition-layout';
import { ExternalId } from 'model/common';
import { objectImagesApi } from 'services/object-images-api';
import { nextPage } from 'utils/next-page';

export const getServerSideProps = async ({ locale = 'ru' }) => ({
  props: await serverSideTranslations(locale, [
    'header',
    'footer',
    'recognition',
    'utils',
    'markup',
  ]),
});

const MarkupById = nextPage(() => {
  const { t } = useI18n('markup');
  const { t: tu } = useI18n('utils');
  const router = useRouter();
  const imageId = router.query.id as string;

  const { data: objectImageIds, isLoading: isObjectImageIdsLoading } = useQuery(
    ['getObjectImageIds'],
    () => objectImagesApi.getObjectImageIds({ size: 10 }),
  );

  const currentIndex =
    objectImageIds?.data.findIndex(id => id === imageId) ?? 0;
  const prev = objectImageIds?.data[currentIndex - 1];
  const next = objectImageIds?.data[currentIndex + 1];

  const changeImage = (externalId?: ExternalId) => {
    if (externalId) {
      router.push(`/recognition/markup/${externalId}`);
    }
  };

  const { data: objectImageUrl, isLoading: isDownloading } = useQuery(
    ['downloadObjectImage', imageId],
    () =>
      objectImagesApi
        .downloadObjectImage(imageId)
        .then(blob => URL.createObjectURL(blob)),
  );

  useEffect(
    () => () => {
      if (objectImageUrl) {
        URL.revokeObjectURL(objectImageUrl);
      }
    },
    [imageId],
  );

  const [markup, setMarkup] = useState<Markup>();
  const [objectClass, setObjectClass] = useState<string>();

  const { data: objectImage, isLoading } = useQuery(
    ['getObjectImage', imageId],
    () => objectImagesApi.getObjectImage(imageId),
    {
      onSuccess: oi => {
        setMarkup(oi.objectFrame);
        setObjectClass(oi.objectFrame?.objectClass);
      },
    },
  );

  const { mutate: save, isLoading: isSaving } = useMutation(
    ['updateObjectFrame', imageId, markup, objectClass],
    () =>
      objectImagesApi.updateObjectFrame(imageId, {
        from: markup!.from,
        to: markup!.to,
        objectClass: objectClass!,
      }),
    {
      onSuccess: () => {
        message.success(t('imageMarkupSuccess'));
      },
      onError: () => {
        message.error(t('imageMarkupError'));
      },
    },
  );

  return (
    <MainLayout contentClassName="sf-markup-page" isPrivate>
      <RecognitionLayout currentStep={1}>
        {isObjectImageIdsLoading || isLoading || isDownloading ? (
          <div className="sf-markup-page__loader">
            <Spin />
          </div>
        ) : (
          <div className="sf-markup-page__content">
            <Alert
              className="sf-markup-page__instruction"
              description={t('imageMarkupInstruction')}
            />
            <div className="sf-markup-page__image-markup">
              <Button
                className="sf-markup-page__arrow"
                type="primary"
                size="large"
                shape="circle"
                icon={<FaChevronLeft />}
                disabled={!prev}
                onClick={() => changeImage(prev)}
              />
              <form
                className="sf-markup-page__form"
                onSubmit={e => e.preventDefault()}
              >
                {objectImage && objectImageUrl && (
                  <ImageMarkup
                    key={JSON.stringify(markup)}
                    image={
                      <img
                        className="sf-markup-page__image"
                        alt={`Image ${router.query.id}`}
                        src={objectImageUrl}
                      />
                    }
                    markup={markup}
                    onMarkup={setMarkup}
                  />
                )}
                <Space>
                  <Input
                    required
                    className="sf-markup-page__input"
                    placeholder={t('inputClass') ?? ''}
                    value={objectClass}
                    onChange={e => setObjectClass(e.target.value)}
                  />
                  <Button
                    type="primary"
                    disabled={!objectClass || !markup}
                    loading={isSaving}
                    onClick={() => save()}
                  >
                    {tu('save')}
                  </Button>
                </Space>
              </form>

              <Button
                className="sf-markup-page__arrow"
                type="primary"
                size="large"
                shape="circle"
                icon={<FaChevronRight />}
                disabled={!next}
                onClick={() => changeImage(next)}
              />
            </div>
          </div>
        )}
      </RecognitionLayout>
    </MainLayout>
  );
});

export default MarkupById;
