import React, { useEffect } from 'react';
import './markup.scss';

import { Spin, useI18n, Typography } from '@ai-lab/ui';
import { useQuery } from '@tanstack/react-query';
import { useRouter } from 'next/router';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

import { MainLayout } from 'components/layout/main-layout';
import { RecognitionLayout } from 'components/layout/recognition-layout';
import { objectImagesApi } from 'services/object-images-api';
import { nextPage } from 'utils/next-page';

export const getStaticProps = async ({ locale = 'ru' }) => ({
  props: await serverSideTranslations(locale, [
    'header',
    'footer',
    'recognition',
    'utils',
    'markup',
  ]),
});

const Markup = nextPage(() => {
  const { t } = useI18n('markup');
  const router = useRouter();

  const { data: objectImageIds, isLoading } = useQuery(
    ['getObjectImageIds'],
    () => objectImagesApi.getObjectImageIds({ size: 1 }),
  );

  const firstImageId = objectImageIds?.data[0];

  useEffect(() => {
    if (firstImageId) {
      router.replace(`/recognition/markup/${firstImageId}`);
    }
  }, [firstImageId]);

  return (
    <MainLayout contentClassName="sf-markup-page" isPrivate>
      <RecognitionLayout currentStep={1}>
        <div className="sf-markup-page__content">
          {isLoading ? (
            <Spin />
          ) : (
            !firstImageId && (
              <Typography.Text className="sf-markup-page__no-images">
                {t('noImages')}
              </Typography.Text>
            )
          )}
        </div>
      </RecognitionLayout>
    </MainLayout>
  );
});

export default Markup;
