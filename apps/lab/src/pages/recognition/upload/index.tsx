import React, { useEffect, useState } from 'react';
import './upload.scss';

import { useI18n, Alert, Spin, UploadGallery } from '@ai-lab/ui';
import { useMutation, useQuery } from '@tanstack/react-query';
import { UploadFile } from 'antd';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { UploadRequestOption, RcFile } from 'rc-upload/lib/interface';

import { MainLayout } from 'components/layout/main-layout';
import { RecognitionLayout } from 'components/layout/recognition-layout';
import { usePageable } from 'hooks/use-pageable';
import { ExternalId } from 'model/common';
import { objectImagesApi } from 'services/object-images-api';
import { nextPage } from 'utils/next-page';

export const getStaticProps = async ({ locale = 'ru' }) => ({
  props: {
    ...(await serverSideTranslations(locale, [
      'header',
      'footer',
      'upload-gallery',
      'recognition',
      'utils',
    ])),
  },
});

const ImageUpload = nextPage(() => {
  const { t } = useI18n('recognition');
  const { pageable } = usePageable();

  const { data: objectImageIds, isLoading } = useQuery(
    ['getObjectImageIds', pageable.page, pageable.size],
    () => objectImagesApi.getObjectImageIds(pageable),
    { cacheTime: 0 },
  );

  const [uploadFiles, setUploadFiles] = useState<UploadFile[]>([]);

  useEffect(() => {
    const controllers: AbortController[] = [];

    if (objectImageIds) {
      const uploadFilesMap = uploadFiles.reduce((acc, el) => {
        acc[el.uid] = el;
        return acc;
      }, {});

      Promise.all(
        objectImageIds.data.map(id => {
          if (uploadFilesMap[id]) {
            return Promise.resolve(uploadFilesMap[id]);
          }

          const controller = new AbortController();
          controllers.push(controller);
          return objectImagesApi
            .downloadObjectImage(id, { signal: controller.signal })
            .then(blob => ({
              uid: id,
              name: id,
              url: URL.createObjectURL(blob),
            }))
            .catch(() => ({
              uid: id,
              name: id,
            }));
        }),
      ).then(setUploadFiles);
    }

    return () => {
      controllers.forEach(controller => controller.abort());

      uploadFiles.forEach(el => {
        if (el.url) {
          URL.revokeObjectURL(el.url);
        }
      });
    };
  }, [objectImageIds]);

  const { mutate: addObjectImage } = useMutation(
    ['addObjectImage'],
    ({ file, onProgress, onSuccess, onError }: UploadRequestOption) =>
      objectImagesApi
        .addObjectImage(file as File, {
          onUploadProgress: e => {
            if (e.total) {
              onProgress?.({
                percent: (e.loaded / e.total) * 100,
              });
            }
          },
        })
        .then(objectImage => {
          setUploadFiles(prev =>
            prev
              .filter(el => el.uid !== (file as RcFile).uid)
              .concat({
                uid: objectImage.id,
                name: objectImage.id,
                url: URL.createObjectURL(file as Blob),
              }),
          );

          onSuccess?.(objectImage);
        })
        .catch(onError),
  );

  const { mutate: deleteObjectImage } = useMutation(
    ['deleteObjectImage'],
    (id: ExternalId) =>
      objectImagesApi
        .deleteObjectImage(id)
        .then(() => setUploadFiles(prev => prev.filter(el => el.uid !== id))),
  );

  return (
    <MainLayout contentClassName="al-upload-page" isPrivate>
      <RecognitionLayout currentStep={0}>
        <div className="al-upload-page__content">
          <Alert
            className="al-upload-page__instruction"
            description={t('imageUploadInstruction')}
          />
          {isLoading ? (
            <Spin className="al-upload-page__spin" />
          ) : (
            <UploadGallery
              className="al-upload-page__gallery"
              upload={{
                fileList: uploadFiles,
                onRemove: file => deleteObjectImage(file.uid),
                customRequest: addObjectImage,
              }}
            />
          )}
        </div>
      </RecognitionLayout>
    </MainLayout>
  );
});

export default ImageUpload;
