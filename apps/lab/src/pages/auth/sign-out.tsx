import { useEffect } from 'react';

import { useRouter } from 'next/router';
import { signOut } from 'next-auth/react';

import { useUser } from 'hooks/use-user';
import { nextPage } from 'utils/next-page';

const SignInPage = nextPage(() => {
  const router = useRouter();
  const { authenticated } = useUser();

  useEffect(() => {
    if (!authenticated) {
      router.push(process.env.NEXT_PUBLIC_DEFAULT_PATH);
    } else {
      signOut({ redirect: false });
    }
  }, [authenticated, router]);

  return null;
});

export default SignInPage;
