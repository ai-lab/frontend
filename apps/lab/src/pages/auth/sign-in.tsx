import { useEffect } from 'react';

import { useRouter } from 'next/router';
import { signIn } from 'next-auth/react';

import { useUser } from 'hooks/use-user';
import { nextPage } from 'utils/next-page';

const SignInPage = nextPage(() => {
  const { authenticated } = useUser();
  const router = useRouter();
  const { callbackUrl = process.env.NEXT_PUBLIC_DEFAULT_PATH } =
    router.query as { callbackUrl: string };

  useEffect(() => {
    if (authenticated) {
      router.push(callbackUrl);
    } else {
      signIn('keycloak', { callbackUrl });
    }
  }, [authenticated, router, callbackUrl]);

  return null;
});

export default SignInPage;
