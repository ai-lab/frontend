import NextAuth, { NextAuthOptions } from 'next-auth';
import { JWT } from 'next-auth/jwt';
import KeycloakProvider from 'next-auth/providers/keycloak';

import { extractRolesFromToken } from 'utils/auth';
import { refreshAccessToken } from 'utils/refersh-access-token';

const notFoundPage = '/404';

export const authOptions: NextAuthOptions = {
  providers: [
    KeycloakProvider({
      clientId: process.env.KEYCLOAK_ID,
      clientSecret: process.env.KEYCLOAK_SECRET,
      issuer: process.env.KEYCLOAK_ISSUER,
      profile: (profile, tokens) => ({
        id: profile.sub,
        username: profile.preferred_username,
        firstname: profile.given_name,
        lastname: profile.family_name,
        email: profile.email,
        emailVerified: profile.email_verified,
        roles: extractRolesFromToken(tokens.access_token),
      }),
    }),
  ],
  pages: {
    signIn: '/auth/sign-in',
    signOut: '/auth/sign-out',
    error: notFoundPage,
    verifyRequest: notFoundPage,
    newUser: notFoundPage,
  },
  callbacks: {
    jwt: ({ token, user, account }) => {
      if (account && user) {
        return {
          accessToken: account.access_token,
          accessTokenExpires: account.expires_at,
          refreshToken: account.refresh_token,
          idToken: account.id_token,
          user,
        } as JWT;
      }

      if (token.accessTokenExpires && Date.now() > token.accessTokenExpires) {
        return refreshAccessToken(token);
      }

      return token;
    },
    session: ({ session, token }) => ({
      ...session,
      user: token.user,
      accessToken: token.accessToken,
      error: token.error,
    }),
  },
};

export default NextAuth(authOptions);
