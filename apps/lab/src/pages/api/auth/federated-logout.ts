import { NextApiRequest, NextApiResponse } from 'next';
import { getToken } from 'next-auth/jwt';

const logoutUrl = `${process.env.NEXT_PUBLIC_BASE_URL}/auth/sign-out`;
const redirectParam = encodeURIComponent(logoutUrl);

export default async function federatedLogout(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  const token = await getToken({ req, secret: process.env.NEXTAUTH_SECRET });
  const idToken = token?.idToken;
  if (!idToken) return res.redirect(logoutUrl);
  return res.redirect(
    // eslint-disable-next-line max-len
    `${process.env.KEYCLOAK_LOGOUT_URL}?post_logout_redirect_uri=${redirectParam}&id_token_hint=${idToken}`,
  );
}
