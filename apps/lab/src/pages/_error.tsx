import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

import { ErrorLayout, ErrorLayoutProps } from 'components/layout/error-layout';
import { nextPage } from 'utils/next-page';

export const getStaticProps = async ({ locale = 'ru', res }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['header', 'footer', 'error'])),
    code: res?.statusCode ?? null,
  },
});

const ErrorPage = nextPage<ErrorLayoutProps>(({ code }) => (
  <ErrorLayout code={code} />
));

export default ErrorPage;
