import createAxiosClient from 'config/axios-config';

export const backendClient = createAxiosClient({
  baseURL: process.env.NEXT_PUBLIC_SERVER_BASE_URL,
});
