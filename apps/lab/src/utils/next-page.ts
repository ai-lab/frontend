import { FC } from 'react';

import { NextPage } from 'next';

export const nextPage = <Props = {}>(render: FC<Props>) =>
  render as unknown as NextPage<Props>;
