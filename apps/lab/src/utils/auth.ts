import jwtDecode from 'jwt-decode';
import { NextRouter } from 'next/router';
import { signIn } from 'next-auth/react';

import { AccessToken, Role } from 'model/auth';

export const login = () => signIn('keycloak');

export const logout = (router: NextRouter) =>
  router.push('/api/auth/federated-logout');

export const getRoles = (roles?: string[]): Role[] => {
  if (!roles) return [];
  const availableRoles = Object.keys(Role);
  return roles
    .filter(role => availableRoles.includes(role))
    .map(role => role as Role);
};

export const extractRolesFromToken = (accessToken?: string): Role[] => {
  if (!accessToken) return [];
  const decoded = jwtDecode<AccessToken>(accessToken);
  return getRoles(decoded.realm_access.roles);
};
