import { JWT } from 'next-auth/jwt';

export const refreshTokenError = 'RefreshTokenError';

const wrapWithError = (token: JWT): JWT => ({
  ...token,
  error: refreshTokenError,
});

export const refreshAccessToken = async (token: JWT): Promise<JWT> => {
  const { refreshToken } = token;
  const { idToken } = token;

  if (!refreshToken) return wrapWithError(token);

  // eslint-disable-next-line max-len
  const tokenUrl = `${process.env.KEYCLOAK_ISSUER}/protocol/openid-connect/token`;

  const details = new URLSearchParams({
    client_id: process.env.KEYCLOAK_ID,
    client_secret: process.env.KEYCLOAK_SECRET,
    grant_type: 'refresh_token',
    refresh_token: refreshToken,
  });

  const response = await fetch(tokenUrl, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
    },
    body: details,
  });

  const refreshedTokens = await response.json();
  if (!response.ok) return wrapWithError(token);

  return {
    ...token,
    accessToken: refreshedTokens.access_token,
    accessTokenExpires: Date.now() + refreshedTokens.expires_in * 1000,
    refreshToken: refreshedTokens.refresh_token ?? refreshToken,
    idToken: refreshedTokens.id_token ?? idToken,
  };
};
