export type ExternalId = string;

export type Pageable = {
  page?: number;
  size?: number;
};

export type PageDto<T> = {
  page: number;
  size: number;
  total: number;
  data: T[];
};
