import { ExternalId } from './common';

export type CoordinatesDto = {
  x: number;
  y: number;
};

export type ObjectFrameDto = {
  from: CoordinatesDto;
  to: CoordinatesDto;
  objectClass: string;
};

export type ObjectImageDto = {
  id: ExternalId;
  objectFrame?: ObjectFrameDto;
};
