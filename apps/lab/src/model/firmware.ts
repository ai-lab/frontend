import { ExternalId } from './common';

export type FirmwareDto = {
  id: ExternalId;
  name: string;
};
