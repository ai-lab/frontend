export type AccessToken = {
  realm_access: {
    roles: string[];
  };
  email_verified: true;
  name: string;
  preferred_username: string;
  given_name: string;
  family_name: string;
  email: string;
};

export enum Role {
  ADMIN = 'ADMIN',
  STUDENT = 'STUDENT',
  TEACHER = 'TEACHER',
}

export type User = {
  username: string;
  firstname: string;
  lastname: string;
  email: string;
  emailVerified: boolean;
  roles: Role[];
};
