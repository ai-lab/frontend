import { QueryClient } from '@tanstack/react-query';

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: process.env.NODE_ENV !== 'development',
      refetchOnWindowFocus: false,
    },
  },
});

export default queryClient;
