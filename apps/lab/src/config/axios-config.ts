import axios, {
  AxiosRequestConfig,
  AxiosResponse,
  CustomParamsSerializer,
  InternalAxiosRequestConfig,
} from 'axios';
import { getSession } from 'next-auth/react';
import qs from 'qs';

const serializeParams: CustomParamsSerializer = params =>
  qs.stringify(params, {
    arrayFormat: 'repeat',
  });

const onRequest =
  (options?: ClientOptions) => async (config: InternalAxiosRequestConfig) => {
    if (options?.authorizeRequest) {
      const session = await getSession();

      if (session?.accessToken) {
        // eslint-disable-next-line no-param-reassign
        config.headers.Authorization = `Bearer ${session.accessToken}`;
      }
    }

    return config;
  };

const onResponse =
  (options?: ClientOptions) => async (response: AxiosResponse) => {
    if (options?.extractResponseData) {
      return response.data;
    }

    return response;
  };

type ClientOptions = {
  authorizeRequest?: boolean;
  extractResponseData?: boolean;
};

const createAxiosClient = (
  config?: AxiosRequestConfig,
  options?: ClientOptions,
) => {
  const client = axios.create({
    paramsSerializer: { serialize: serializeParams },
    ...config,
  });

  const clientOptions = {
    authorizeRequest: true,
    extractResponseData: true,
    catchCanceledError: true,
    ...options,
  };

  client.interceptors.request.use(onRequest(clientOptions));
  client.interceptors.response.use(onResponse(clientOptions));
  return client;
};

export default createAxiosClient;
