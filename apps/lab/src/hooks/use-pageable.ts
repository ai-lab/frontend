import { useState } from 'react';

import { Pageable } from 'model/common';

export const defaultPageable: Pageable = {
  page: 0,
  size: 10,
};

export const usePageable = (initialPageable?: Pageable) => {
  const [page, setPage] = useState(
    initialPageable?.page ?? defaultPageable.page,
  );

  const [size, setSize] = useState(
    initialPageable?.size ?? defaultPageable.size,
  );

  const pageable: Pageable = {
    page,
    size,
  };

  return {
    pageable,
    setPage,
    setSize,
  };
};
