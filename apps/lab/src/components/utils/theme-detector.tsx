import { useEffect } from 'react';

import {
  useTheme,
  Theme,
  component,
  WithChildren,
  detectTheme,
} from '@ai-lab/ui';
import { useTheme as useNextTheme } from 'next-themes';

export const ThemeDetector = component<WithChildren>(({ children }) => {
  const { resolvedTheme } = useNextTheme();
  const { setTheme } = useTheme();

  useEffect(() => {
    setTheme((resolvedTheme as Theme) ?? detectTheme());
  }, [resolvedTheme]);

  // eslint-disable-next-line react/jsx-no-useless-fragment
  return <>{children}</>;
});
