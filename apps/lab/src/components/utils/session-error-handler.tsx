import { FC, useEffect } from 'react';

import { WithChildren } from '@ai-lab/ui';
import { useRouter } from 'next/router';
import { useSession } from 'next-auth/react';

import { logout } from 'utils/auth';
import { refreshTokenError } from 'utils/refersh-access-token';

export const SessionErrorHandler: FC<WithChildren> = ({ children }) => {
  const router = useRouter();
  const { error: queryError } = router.query as { error: string | undefined };
  const { data } = useSession();

  const errorHandlers = {
    [refreshTokenError]: () => logout(router),
    OAuthSignin: () => {
      if (window.location.pathname !== process.env.NEXT_PUBLIC_DEFAULT_PATH) {
        window.location.href = process.env.NEXT_PUBLIC_DEFAULT_PATH;
      }
    },
  };

  useEffect(() => {
    const error = data?.error ?? queryError;

    if (error) {
      // eslint-disable-next-line no-console
      console.error(`Session error: ${error}`);
      errorHandlers[error]?.();
    }
  }, [queryError, data]);

  // eslint-disable-next-line react/jsx-no-useless-fragment
  return <>{children}</>;
};
