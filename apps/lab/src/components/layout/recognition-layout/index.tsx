import './recognition-layout.scss';

import { component, Steps, WithChildren, useI18n, Button } from '@ai-lab/ui';
import { useRouter } from 'next/router';

export type RecognitionLayoutProps = WithChildren<{
  currentStep: number;
}>;

const steps = [
  {
    path: '/upload',
    titleKey: 'imageUpload',
  },
  {
    path: '/markup',
    titleKey: 'imageMarkup',
  },
  {
    path: '/training',
    titleKey: 'networkTraining',
  },
  {
    path: '/testing',
    titleKey: 'networkTesting',
  },
];

export const RecognitionLayout = component<RecognitionLayoutProps>(
  ({ currentStep, children }) => {
    const router = useRouter();
    const { t } = useI18n('recognition');
    const { t: tu } = useI18n('utils');

    const changeStep = (index: number) => {
      if (steps[index]) {
        router.push(`/recognition${steps[index].path}`);
      }
    };

    return (
      <div className="al-recognition-steps">
        <Steps
          className="al-recognition-steps__header"
          current={currentStep}
          onChange={changeStep}
          items={steps.map(step => ({
            title: t(step.titleKey),
          }))}
        />
        <div className="al-recognition-steps__content">{children}</div>
        <div className="al-recognition-steps__footer">
          {currentStep !== 0 && (
            <Button onClick={() => changeStep(currentStep - 1)}>
              {tu('backward')}
            </Button>
          )}
          {currentStep !== steps.length - 1 && (
            <Button
              type="primary"
              className="al-recognition-steps__footer-forward"
              onClick={() => changeStep(currentStep + 1)}
            >
              {tu('forward')}
            </Button>
          )}
        </div>
      </div>
    );
  },
);
