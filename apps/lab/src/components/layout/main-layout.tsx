import { useEffect } from 'react';

import {
  component,
  PageLayout,
  useI18n,
  Locale,
  WithChildren,
} from '@ai-lab/ui';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useTheme } from 'next-themes';

import { AiLabHead } from 'components/layout/ai-lab-head';
import { useUser } from 'hooks/use-user';
import { login, logout } from 'utils/auth';

export type MainLayoutProps = WithChildren<{
  isPrivate?: boolean;
  contentClassName?: string;
}>;

export const MainLayout = component<MainLayoutProps>(
  ({ isPrivate, contentClassName, children }) => {
    const { t } = useI18n('header');
    const router = useRouter();
    const { setTheme } = useTheme();
    const { user, authenticated } = useUser();

    useEffect(() => {
      if (isPrivate && !authenticated) {
        router.push('/auth/sign-in');
      }
    }, [isPrivate && authenticated]);

    const redirectToLocale = (locale: Locale) => {
      const { pathname, asPath, query } = router;
      router.push({ pathname, query }, asPath, { locale });
    };

    const menuItems = [
      {
        key: '/recognition',
        label: t('imageRecognition'),
      },
    ];

    const selectedItems = menuItems.filter(item =>
      router.pathname.startsWith(item.key),
    );

    return (
      <>
        <AiLabHead subtitles={selectedItems.map(item => item.label)} />
        <PageLayout
          contentClassName={contentClassName}
          header={{
            titleLink: <Link href={process.env.NEXT_PUBLIC_DEFAULT_PATH} />,
            menu: {
              items: menuItems.map(item => ({
                ...item,
                onClick: () => router.push(item.key),
              })),
              selectable: true,
              selectedKeys: selectedItems.map(item => item.key),
            },
            themeSwitch: {
              onThemeChange: setTheme,
            },
            localeDropdown: {
              onLocaleChange: redirectToLocale,
            },
            onLogin: login,
            onLogout: () => logout(router),
            loggedIn: authenticated,
            username: user && `${user.firstname} ${user.lastname[0]}.`,
          }}
        >
          {children}
        </PageLayout>
      </>
    );
  },
);
