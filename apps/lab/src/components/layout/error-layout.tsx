import { Error, component } from '@ai-lab/ui';
import { useRouter } from 'next/router';

import { MainLayout } from 'components/layout/main-layout';

export type ErrorLayoutProps = {
  code?: number;
};

export const ErrorLayout = component<ErrorLayoutProps>(({ code }) => {
  const router = useRouter();

  return (
    <MainLayout>
      <Error
        code={code}
        onHomeClick={() => router.push(process.env.NEXT_PUBLIC_DEFAULT_PATH)}
      />
    </MainLayout>
  );
});
