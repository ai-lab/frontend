import { DefaultSession, User } from 'next-auth';

import { User as AiLabUser } from '../src/model/auth';

declare module 'next-auth' {
  interface User extends AiLabUser {}

  interface Session extends DefaultSession {
    user?: User;
    accessToken?: string;
    error?: string;
  }
}

declare module 'next-auth/jwt' {
  interface JWT {
    accessToken?: string;
    accessTokenExpires?: number;
    refreshToken?: string;
    idToken?: string;
    user?: User;
    error?: string;
  }
}
