/**
 * Types for environmental variables.
 * See .env files for more information.
 */
export declare global {
  namespace NodeJS {
    type Env = {
      NEXT_PUBLIC_BASE_URL: string;
      NEXT_PUBLIC_SERVER_BASE_URL: string;
      NEXTAUTH_URL: string;
      NEXTAUTH_SECRET?: string;
      KEYCLOAK_ID: string;
      KEYCLOAK_SECRET: string;
      KEYCLOAK_ISSUER: string;
      KEYCLOAK_LOGOUT_URL: string;
      NEXT_PUBLIC_SESSION_REFETCH_INTERVAL: number;
      NEXT_PUBLIC_DEFAULT_PATH: string;
    };

    interface ProcessEnv extends NodeJS.ProcessEnv, Readonly<Env> {}
  }
}
