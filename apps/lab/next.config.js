const path = require('path');

const { i18n } = require('./next-i18next.config');

/** @type {import('next').NextConfig} */
module.exports = {
  i18n,

  webpack: config => {
    config.module.rules.forEach(rule => {
      const { oneOf } = rule;
      if (oneOf) {
        oneOf.forEach(one => {
          if (!`${one.issuer?.and}`.includes('_app')) return;
          // eslint-disable-next-line no-param-reassign
          one.issuer.and = [path.resolve(__dirname)];
        });
      }
    });

    config.module.rules.push({
      test: /\.svg$/i,
      issuer: /\.[jt]sx?$/,
      use: {
        loader: '@svgr/webpack',
        options: {
          svgoConfig: {
            plugins: [{ name: 'removeViewBox', active: false }],
          },
        },
      },
    });

    return config;
  },

  images: {
    remotePatterns: [
      {
        protocol: 'http',
        hostname: 'localhost',
        port: '8080',
        pathname: '/api/v1/object-images/*/preview',
      },
    ],
  },

  redirects: async () =>
    [
      {
        source: '/',
        destination: process.env.NEXT_PUBLIC_DEFAULT_PATH,
        permanent: false,
      },
    ].filter(redirect => redirect.source !== redirect.destination),
};
