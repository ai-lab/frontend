const { availableNamespaces } = require('@ai-lab/ui');
const fs = typeof window === 'undefined' ? require('fs') : undefined;

/** @type { import('next-i18next').UserConfig } */
module.exports = {
  i18n: {
    defaultLocale: 'ru',
    locales: ['ru', 'en', 'cn'],
  },
  fallbackLng: 'en',
  ns: [
    'home',
    'markup',
    'recognition',
    'testing',
    'training',
    'utils',
    ...availableNamespaces,
  ],
  localePath: (lng, ns) => {
    const path = `./public/locales/${lng}/${ns}.json`;
    if (fs?.existsSync(path)) return path;
    return `../../packages/ui/public/locales/${lng}/${ns}.json`;
  },
  serializeConfig: false,
  reloadOnPrerender: process.env.NODE_ENV === 'development',
};
